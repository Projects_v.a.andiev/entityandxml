using EntityAndXML.FolderFileOperation;
using EntityAndXML.Helper;
using EntityAndXML.Model;
using EntityAndXML.OperationsWithRequest;
using EntityAndXMLTest.Helper;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EntityAndXMLTest
{
    [TestClass]
    public class ParallelParsingXml
    {
        private ILog _log;
        /// <summary>
        /// ��������� ���� ��� � �������� ������ ������
        /// </summary>
        /// <param name="context"></param>
        [ClassInitialize]
        public static void InitializeTest(TestContext context)
        {
            var loggerInstance = new Logger().InitializeLogger();
            var checkingParsingXml = new ParallelParsingXml();
            checkingParsingXml._log = loggerInstance;
        }

        /// <summary>
        /// ������ XML ������ � �������� ������� ������ � ����������� moq ��,   
        /// </summary>
        [TestMethod]
        public void Parse_4005_Request_AndTestInsertInDb()
        {

            var mockContext = new Mock<SupportRequestDbContext>();

            //������������� SRV ��� ��� ��� ��������� RequestSupport ��������� ������ � ������� SRV

            var listSrv = new List<SRV>() { new SRV() { ID = 0 } };
            var moqHelper = new MoqHelper();
            var mockSRV = moqHelper.GetQueryableMockDbSet(listSrv);

            mockContext.Setup(c => c.SRV).Returns(mockSRV);

            var repository = new RepositoryModel(mockContext.Object);

            var parseRequests = new ParseRequests(repository, _log);

            var supportRequests = parseRequests.GetRequests(PathSettings.PathForFolderRequests(), false);

            var req = parseRequests.ListSupportRequest(supportRequests);
            //������������� RequestSupport ����� ����� ���� �������� �������� �� ������� RequestSupport

            var mockSupportRequests = moqHelper.GetQueryableMockDbSet(req);

            mockContext.Setup(c => c.RequestSupport).Returns(mockSupportRequests);

            //���� ���������� �������� ����� ������ ���������� �� ����� ��������� ������� � ������ ����� ��� �� �������� ��
            var countReq = mockContext.Object.RequestSupport.Count();

            //�������� ���� ��� ���������� � �������� � �� ���������� ���������� �������.
            Assert.AreEqual(countReq ,4005 );
            Assert.AreNotEqual(countReq,  0);
        }

        /// <summary>
        /// �������� ������������� �� ���������� ���� ������ xml � �� �������������� � ��������� �� ������ ���� �� ����� ����������� ��� � ������ �� �����.
        /// ��� ��� ����� �� ��� ��������� ����� ��������� � �� ������� AddRange ������� ��������� ��������� � ��� ������ ���� ���������� ���������� ��������.
        /// </summary>
        [TestMethod]
        public void Parse_4005_xmls()
        {
            var mock = new Mock<SupportRequestDbContext>();
            //���������� �����������
            var repository = new RepositoryModel(mock.Object);
            var parseRequests = new ParseRequests(repository, _log);
            var requests = parseRequests.GetRequests(PathSettings.PathForFolderRequests(), false);

            var countsReq = requests.Count();
            //�������� ���� ��� � �������� ��������� �������� ����� ������� �������� ������� ���������� ��� ������������ ���������.
            Assert.AreEqual(countsReq, 4005);
            Assert.AreNotEqual(countsReq , 0);
        }

        /// <summary>
        /// �������� ���� ��� ��������� CI � Srv ���� ����������� � ���������� ���������� ��������� ���������.
        /// ����������� ������ � �� � �������� � ����������� ����������.
        /// </summary>
        [TestMethod]
        public void Test_Proccesing_Operation_CI_Srv()
        {
            var mock = new Mock<SupportRequestDbContext>();
            var listOrganization = new List<Organization>() { new Organization() { ID = 0 } };
            var moqHelper = new MoqHelper();
            var mockOrganization = moqHelper.GetQueryableMockDbSet(listOrganization);

            mock.Setup(c => c.Organization).Returns(mockOrganization);
            //���������� �����������
            var repository = new RepositoryModel(mock.Object);
            var parseRequests = new ParseOrgSrv(repository, _log);

            var requests = parseRequests.GetRequests(PathSettings.PathORG_SRV(), false);

            var organizations = moqHelper.GetQueryableMockDbSet(parseRequests.ParseOrganization(requests));
            mock.Setup(c => c.Organization).Returns(organizations);

            var indexCi = 0;
            var indexSrv = 0;

            var taskCI = Task.Factory.StartNew(() =>
                mock.Setup(c => c.ConfigurationItem).Returns(
                    moqHelper.GetQueryableMockDbSet(parseRequests.ParseConfigurationItem(requests, ref indexCi).Values.ToList())));

            var taskSRV = Task.Factory.StartNew(() =>
            mock.Setup(c => c.SRV).Returns(moqHelper.GetQueryableMockDbSet(parseRequests.ParseSRV(requests, ref indexSrv).Values.ToList())));

            Task.WaitAll(taskCI, taskSRV);

            var rep = new RepositoryModel(mock.Object);

            Assert.AreEqual(mock.Object.Organization.Count() , 1);
            Assert.AreEqual(mock.Object.ConfigurationItem.Count() , 5);
            Assert.AreEqual(mock.Object.SRV.Count() , 12);
        }
    }
}
