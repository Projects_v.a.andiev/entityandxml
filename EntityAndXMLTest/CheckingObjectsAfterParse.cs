﻿using EntityAndXML.FolderFileOperation;
using EntityAndXML.Helper;
using EntityAndXML.Model;
using EntityAndXML.OperationsWithRequest;
using EntityAndXML.Request;
using EntityAndXMLTest.Helper;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace EntityAndXMLTest
{
    /// <summary>
    ////Проверка того как прошла обработка xml и что полученый Объект соотвествует данным в xml
    /// </summary>
    [TestClass]
    public class CheckingObjectsAfterParse
    {
        private ILog _log;
        /// <summary>
        /// Настройка один раз в тестовом классе класса
        /// </summary>
        /// <param name="context"></param>
        [ClassInitialize]
        public static void InitializeTest(TestContext context)
        {
            var loggerInstance = new Logger().InitializeLogger();
            var prog = new CheckingObjectsAfterParse();
            prog._log = loggerInstance;
        }

        [TestMethod]
        public void Test_Organization()
        {
            var mock = new Mock<SupportRequestDbContext>();



            //Подготовка репозитория
            var repository = new RepositoryModel(mock.Object);
            var parseRequests = new ParseOrgSrv(repository, _log);

            var req = parseRequests.GetRequests(PathSettings.PathORG_SRV(), false);

            var orgs = parseRequests.ParseOrganization(req).FirstOrDefault();

            Assert.AreEqual(orgs.ORG_NAME, "Клиент у которого обслуживаем технику");
            Assert.AreEqual(orgs.ORG_OID, 282);
            Assert.AreEqual(orgs.ORG_SEARCHCODE, "КЛИЕНТ-1");
            Assert.AreEqual(orgs.RCT_NAME, "Заказчик");
        }

        [TestMethod]
        public void Test_ConfigurationItem()
        {
            var mock = new Mock<SupportRequestDbContext>();
            var listOrganization = new List<Organization>() { new Organization() { ID = 0 } };
            var moqHelper = new MoqHelper();
            var mockOrganization = moqHelper.GetQueryableMockDbSet(listOrganization);

            mock.Setup(c => c.Organization).Returns(mockOrganization);
            //Подготовка репозитория
            var repository = new RepositoryModel(mock.Object);
            var parseRequests = new ParseOrgSrv(repository, _log);

            var req = parseRequests.GetRequests(PathSettings.PathORG_SRV(), false);
            var i = 0;
            var orgs = parseRequests.ParseConfigurationItem(req, ref i).Values.Where(w => w.ID_CI == 4).FirstOrDefault();

            Assert.AreEqual(orgs.NAME, "Краснодар_Тургенева");
            Assert.AreEqual(orgs.TOWN, "Краснодар");
            Assert.AreEqual(orgs.MODEL, "HP LaserJet Pro M426");
            Assert.AreEqual(orgs.TID, "NULL");
            Assert.AreEqual(orgs.SERNUM, "PHB8K2015P");
            Assert.AreEqual(orgs.SC, "MFU-STANDART-2");
            Assert.AreEqual(orgs.CAT, "МФУ");
            Assert.AreEqual(orgs.ORG_OID, 282);
            Assert.AreEqual(orgs.BLOCKED, "0");
        }

        [TestMethod]
        public void Test_SRV()
        {
            var mock = new Mock<SupportRequestDbContext>();
            var listOrganization = new List<Organization>() { new Organization() { ID = 0 } };
            var moqHelper = new MoqHelper();
            var mockOrganization = moqHelper.GetQueryableMockDbSet(listOrganization);

            mock.Setup(c => c.Organization).Returns(mockOrganization);
            //Подготовка репозитория
            var repository = new RepositoryModel(mock.Object);
            var parseRequests = new ParseOrgSrv(repository, _log);

            var req = parseRequests.GetRequests(PathSettings.PathORG_SRV(), false);
            var i = 0;
            var orgs = parseRequests.ParseSRV(req, ref i).Values.Where(w => w.SRV_ID == 478).FirstOrDefault();

            Assert.AreEqual(orgs.SRV_NAME, "АРМ: Клиент");
            Assert.AreEqual(orgs.PATH, @"\АРМ: МФУ");
            Assert.AreEqual(orgs.ORG_OID, 282);

        }


        [TestMethod]
        public void Test_GetOrganizationID()
        {
            var mock = new Mock<SupportRequestDbContext>();
            var listOrganization = new List<Organization>()
            {
                new Organization()  { ID = 0 } ,
                new Organization() { ID = 1 },
                new Organization() { ID = 2 }
            };

            var moqHelper = new MoqHelper();
            var mockOrganization = moqHelper.GetQueryableMockDbSet(listOrganization);

            mock.Setup(c => c.Organization).Returns(mockOrganization);

            Assert.AreEqual(mock.Object.Organization.Where(w => w.ID == 0).Select(x => x.ID).FirstOrDefault(), 0);
            Assert.AreEqual(mock.Object.Organization.Where(w => w.ID == 1).Select(x => x.ID).FirstOrDefault(), 1);
            Assert.AreEqual(mock.Object.Organization.Where(w => w.ID == 2).Select(x => x.ID).FirstOrDefault(), 2);
            Assert.AreNotEqual(mock.Object.Organization.Where(w => w.ID == 555).Select(x => x.ID).FirstOrDefault(), 555);
        }

        /// <summary>
        /// Пока метод получения Id берет значения из БД так что нужно продумать как обойти этот момент 
        /// </summary>
        [TestMethod]
        public void Test_FkForRequestSupportSrvIdCiId()
        {
            var mock = new Mock<SupportRequestDbContext>();
            var listServ = new List<SRV>()
            {
                new SRV() { ID = 2, SRV_ID = 479 },                
                new SRV() { ID = 5, SRV_ID = 481 },
                new SRV() { ID = 9, SRV_ID = 483 },
                new SRV() { ID = 10, SRV_ID = 484 },
                new SRV() { ID = 12, SRV_ID = 488 }
            };

            var moqHelper = new MoqHelper();
            var mockSrv = moqHelper.GetQueryableMockDbSet(listServ);

            mock.Setup(c => c.SRV).Returns(mockSrv);

            var listConfigurationItem = new List<ConfigurationItem>()
            {
                new ConfigurationItem() { ID = 1, SC = "MFU-STANDART-1" },
                new ConfigurationItem() { ID = 2, SC = "MFU-STANDART-2" },
                new ConfigurationItem() { ID = 3, SC = "MFU-STANDART-3" },
                new ConfigurationItem() { ID = 4, SC = "MFU-STANDART-4" },
                new ConfigurationItem() { ID = 5, SC = "MFU-STANDART-5" }
            };

            var mockConfigurationItem = moqHelper.GetQueryableMockDbSet(listConfigurationItem);

            mock.Setup(c => c.ConfigurationItem).Returns(mockConfigurationItem);


            var repository = new RepositoryModel(mock.Object);

            var parseRequests = new ParseRequests(repository, _log);

            var supportRequests = parseRequests.GetRequests(PathSettings.PathForFolderRequests(), false);

            var req = parseRequests.ListSupportRequest(supportRequests);
            //Инициализация RequestSupport чтобы можно было получить значения из таблицы RequestSupport

            var mockSupportRequests = moqHelper.GetQueryableMockDbSet(req);

            mock.Setup(c => c.RequestSupport).Returns(mockSupportRequests);

            var requestsNewFk = parseRequests.FkForRequestSupportSrvIdCiId(mock.Object.RequestSupport.ToList());

            var rr = requestsNewFk.GroupBy(w=> w.SRV_ID);

            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 479).Select(x => x.SrvId).FirstOrDefault(), 2);
            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 479).Select(x => x.ConfigurationItemId).FirstOrDefault(), 1);

            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 481).Select(x => x.SrvId).FirstOrDefault(), 5);
            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 481).Select(x => x.ConfigurationItemId).FirstOrDefault(), 3);

            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 483).Select(x => x.SrvId).FirstOrDefault(), 9);
            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 483).Select(x => x.ConfigurationItemId).FirstOrDefault(), 4);

            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 484).Select(x => x.SrvId).FirstOrDefault(), 10);
            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 484).Select(x => x.ConfigurationItemId).FirstOrDefault(), 4);

            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 488).Select(x => x.SrvId).FirstOrDefault(), 12);
            Assert.AreEqual(requestsNewFk.Where(w => w.SRV_ID == 488).Select(x => x.ConfigurationItemId).FirstOrDefault(), 2);

            Assert.AreNotEqual(requestsNewFk.Where(w => w.SRV_ID == 479).Select(x => x.SrvId).FirstOrDefault(), null);
            Assert.AreNotEqual(requestsNewFk.Where(w => w.SRV_ID == 479).Select(x => x.ConfigurationItemId).FirstOrDefault(), null);

            Assert.AreNotEqual(requestsNewFk.Where(w => w.SRV_ID == 488).Select(x => x.SrvId).FirstOrDefault(), 100);
            Assert.AreNotEqual(requestsNewFk.Where(w => w.SRV_ID == 488).Select(x => x.ConfigurationItemId).FirstOrDefault(), null);

            Assert.AreNotEqual(requestsNewFk.Where(w => w.SRV_ID == 481).Select(x => x.SrvId).FirstOrDefault(), 100);
            Assert.AreNotEqual(requestsNewFk.Where(w => w.SRV_ID == 481).Select(x => x.ConfigurationItemId).FirstOrDefault(), null);
        }




        [TestMethod]
        public void Test_GetSrvId()
        {
            var mock = new Mock<SupportRequestDbContext>();
            var listSrv = new List<SRV>()
            {
                new SRV()  { ID = 0 } ,
                new SRV() { ID = 1 },
                new SRV() { ID = 2 }
            };

            var moqHelper = new MoqHelper();
            var mockSrv = moqHelper.GetQueryableMockDbSet(listSrv);

            mock.Setup(c => c.SRV).Returns(mockSrv);

            Assert.AreEqual(mock.Object.SRV.Where(w => w.ID == 0).Select(x => x.ID).FirstOrDefault(), 0);
            Assert.AreEqual(mock.Object.SRV.Where(w => w.ID == 1).Select(x => x.ID).FirstOrDefault(), 1);
            Assert.AreEqual(mock.Object.SRV.Where(w => w.ID == 2).Select(x => x.ID).FirstOrDefault(), 2);
            Assert.AreNotEqual(mock.Object.SRV.Where(w => w.ID == 555).Select(x => x.ID).FirstOrDefault(), 555);
        }


        [TestMethod]
        public void Test_GetConfugurationItemId()
        {
            var mock = new Mock<SupportRequestDbContext>();
            var listConfigurationItem = new List<ConfigurationItem>()
            {
                new ConfigurationItem() { ID = 0 },
                new ConfigurationItem() { ID = 1 },
                new ConfigurationItem() { ID = 2 }
            };

            var moqHelper = new MoqHelper();
            var mockConfigurationItem = moqHelper.GetQueryableMockDbSet(listConfigurationItem);

            mock.Setup(c => c.ConfigurationItem).Returns(mockConfigurationItem);

            Assert.AreEqual(mock.Object.ConfigurationItem.Where(w => w.ID == 0).Select(x => x.ID).FirstOrDefault(), 0);
            Assert.AreEqual(mock.Object.ConfigurationItem.Where(w => w.ID == 1).Select(x => x.ID).FirstOrDefault(), 1);
            Assert.AreEqual(mock.Object.ConfigurationItem.Where(w => w.ID == 2).Select(x => x.ID).FirstOrDefault(), 2);
            Assert.AreNotEqual(mock.Object.ConfigurationItem.Where(w => w.ID == 555).Select(x => x.ID).FirstOrDefault(), 555);
        }
    }
}
