﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityAndXML.Migrations
{
    public partial class v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Organization",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_ORG = table.Column<int>(nullable: false),
                    ORG_NAME = table.Column<string>(nullable: true),
                    ORG_SEARCHCODE = table.Column<string>(nullable: true),
                    ORG_OID = table.Column<int>(nullable: false),
                    RCT_NAME = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organization", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ConfigurationItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_CI = table.Column<int>(nullable: false),
                    NAME = table.Column<string>(nullable: true),
                    TOWN = table.Column<string>(nullable: true),
                    ADR = table.Column<string>(nullable: true),
                    MODEL = table.Column<string>(nullable: true),
                    TID = table.Column<string>(nullable: true),
                    SERNUM = table.Column<string>(nullable: true),
                    SC = table.Column<string>(nullable: true),
                    CAT = table.Column<string>(nullable: true),
                    ORG_OID = table.Column<int>(nullable: false),
                    BLOCKED = table.Column<string>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfirationItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ConfigurationItem_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organization",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SRV",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_SRV = table.Column<int>(nullable: false),
                    SRV_NAME = table.Column<string>(nullable: true),
                    SRV_ID = table.Column<int>(nullable: false),
                    PATH = table.Column<string>(nullable: true),
                    ORG_OID = table.Column<int>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SRV", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SRV_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organization",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RequestSupport",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_HD = table.Column<string>(nullable: true),
                    INFO = table.Column<string>(nullable: true),
                    SRV_ID = table.Column<int>(nullable: false),
                    EQUIPMENT = table.Column<string>(nullable: true),
                    SC = table.Column<string>(nullable: true),
                    NEW_ROOM = table.Column<string>(nullable: true),
                    DATE_REG_CLIENT = table.Column<string>(nullable: true),
                    DATE_REG = table.Column<string>(nullable: true),
                    DESIRED_TIME = table.Column<string>(nullable: true),
                    SrvId = table.Column<int>(nullable: true),
                    ConfigurationItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestSupport", x => x.ID);
                    table.ForeignKey(
                        name: "FK_RequestSupport_ConfigurationItem_ConfigurationItemId",
                        column: x => x.ConfigurationItemId,
                        principalTable: "ConfigurationItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RequestSupport_SRV_SrvId",
                        column: x => x.SrvId,
                        principalTable: "SRV",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConfirationItem_OrganizationId",
                table: "ConfigurationItem",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "FK_RequestSupport_ConfigurationItem_ConfigurationItemId",
                table: "RequestSupport",
                column: "ConfigurationItemId");

            migrationBuilder.CreateIndex(
                name: "FK_RequestSupport_SRV_SrvId",
                table: "RequestSupport",
                column: "SrvId");

            migrationBuilder.CreateIndex(
                name: "IX_SRV_OrganizationId",
                table: "SRV",
                column: "OrganizationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RequestSupport");

            migrationBuilder.DropTable(
                name: "ConfigurationItem");

            migrationBuilder.DropTable(
                name: "SRV");

            migrationBuilder.DropTable(
                name: "Organization");
        }
    }
}
