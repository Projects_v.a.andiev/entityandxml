﻿using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;


namespace EntityAndXML.FolderFileOperation
{
    public static class PathSettings
    {
        public static string PathForFolderRequests()
        {
            var startDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            return startDirectory = $@"{startDirectory}/ServiceRequest/IN/";
        }

        public static string PathORG_SRV()
        {
            var startDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            startDirectory = $@"{startDirectory}/ServiceRequest/ORG_SVR/";
            return startDirectory;
        }

        public static string MsSqlConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MsSqlConnectionString"].ConnectionString;
        }

    }
}
