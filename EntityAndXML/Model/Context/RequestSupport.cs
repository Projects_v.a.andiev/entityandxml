﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityAndXML.Model
{
    public class RequestSupport
    {
        public int ID { get; set; }

        public string ID_HD { get; set; }
        public string INFO { get; set; }
        public int SRV_ID { get; set; }

        public string EQUIPMENT { get; set; }

        public string SC { get; set; }

        public string NEW_ROOM { get; set; }

        public string DATE_REG_CLIENT { get; set; }

        public string DATE_REG { get; set; }

        public string DESIRED_TIME { get; set; }

        public int? SrvId { get; set; }
        public SRV RequestSupportSrvIdNavigation { get; set; }

        public int? ConfigurationItemId { get; set; }
        public ConfigurationItem RequestConfigurationItemIdNavigation { get; set; }
    }
}
