﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityAndXML.Model
{
    public class Organization
    {
        public int ID { get; set; }
        public int ID_ORG { get; set; }

        public string ORG_NAME { get; set; }

        public string ORG_SEARCHCODE { get; set; }
        
        public int ORG_OID { get; set; }

        public string RCT_NAME { get; set; }

        public ICollection<SRV> OrganizationSrvNavigation { get; set; }

        public ICollection<ConfigurationItem > OrganizationCiIdNavigation { get; set; }   

    }
}
