﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EntityAndXML.Model
{
    public class ConfigurationItem
    {
        public int ID { get; set; }
        public int ID_CI { get; set; }
        public string NAME { get; set; }
        public string TOWN { get; set; }
        public string ADR { get; set; }
        public string MODEL { get; set; }
        public string TID { get; set; }
        public string SERNUM { get; set; }
        public string SC { get; set; }
        public string CAT { get; set; }
        public int ORG_OID { get; set; }
        public string BLOCKED { get; set; }

        public int? OrganizationId { get; set; }
 
        public Organization CIOrganizationIdNavigation { get; set; }
        public ICollection<RequestSupport> CiRequestSupportIdNavigation { get; set; }

    }
}
