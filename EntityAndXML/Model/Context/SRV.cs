﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityAndXML.Model
{
    public class SRV
    {
        public int ID { get; set; }
        public int ID_SRV { get; set; }
        public string SRV_NAME { get; set; }
        public int SRV_ID { get; set; }
        public string PATH { get; set; }
        public int ORG_OID { get; set; }

        public int? OrganizationId { get; set; }

        public Organization SRVOrganizationIdNavigation { get; set; }

        public ICollection<RequestSupport> SrvRequestSupportNavigation { get; set; }
    }
}
