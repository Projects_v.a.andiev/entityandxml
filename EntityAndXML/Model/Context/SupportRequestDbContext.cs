﻿using EntityAndXML.FolderFileOperation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace EntityAndXML.Model
{
    public class SupportRequestDbContext : DbContext
    {
        public SupportRequestDbContext()
        {
        }

        public SupportRequestDbContext(DbContextOptions<SupportRequestDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                .UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=TestDB;Trusted_Connection=True;");
            }
        }

        public virtual DbSet<ConfigurationItem> ConfigurationItem { get; set; }
        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<RequestSupport> RequestSupport { get; set; }
        public virtual DbSet<SRV> SRV { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<SRV>(entity =>
            {
                entity.HasKey(e => e.ID)
                  .HasName("PK_SRV");

                entity.HasIndex(e => e.OrganizationId)
                .HasName("IX_SRV_OrganizationId");

                entity.HasOne(d => d.SRVOrganizationIdNavigation)
                     .WithMany(p => p.OrganizationSrvNavigation)
                     .HasForeignKey(d => d.OrganizationId)
                     .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Organization>(entity =>
            {
                entity.HasKey(e => e.ID)
                  .HasName("PK_Organization");

                entity.HasMany(d => d.OrganizationSrvNavigation)
                     .WithOne(p => p.SRVOrganizationIdNavigation)
                     .HasForeignKey(d => d.OrganizationId)
                     .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasMany(d => d.OrganizationCiIdNavigation)
                     .WithOne(p => p.CIOrganizationIdNavigation)
                     .HasForeignKey(d => d.OrganizationId)
                     .OnDelete(DeleteBehavior.ClientSetNull);
        
            });


            modelBuilder.Entity<ConfigurationItem>(entity =>
           {
               entity.HasKey(e => e.ID)
                   .HasName("PK_ConfirationItem");

               entity.HasIndex(e => e.OrganizationId)
                   .HasName("IX_ConfirationItem_OrganizationId");


               entity.HasOne(d => d.CIOrganizationIdNavigation)
                    .WithMany(p => p.OrganizationCiIdNavigation)
                    .HasForeignKey(d => d.OrganizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
           });

            modelBuilder.Entity<RequestSupport>(entity =>
            {
                entity.HasKey(e => e.ID)
                    .HasName("PK_RequestSupport");

                entity.HasIndex(e => e.ConfigurationItemId)
                    .HasName("FK_RequestSupport_ConfigurationItem_ConfigurationItemId");

                entity.HasIndex(e => e.SrvId)
                    .HasName("FK_RequestSupport_SRV_SrvId");

                entity.HasOne(d => d.RequestConfigurationItemIdNavigation)
                     .WithMany(p => p.CiRequestSupportIdNavigation)
                     .HasForeignKey(d => d.ConfigurationItemId)
                     .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.RequestSupportSrvIdNavigation)
                     .WithMany(p => p.SrvRequestSupportNavigation)
                     .HasForeignKey(d => d.SrvId)
                     .OnDelete(DeleteBehavior.ClientSetNull);
            });
        }
    }
}