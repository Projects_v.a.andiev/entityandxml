﻿using EntityAndXML.Model.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityAndXML.Model
{
    public class RepositoryModel : IOrganization, IConfirationItem, IRequestSupport, ISrv
    {
        private SupportRequestDbContext context;

        public RepositoryModel(SupportRequestDbContext ctx)
        {
            context = ctx;
        }



        public async Task<List<Organization>> OrganizationsListAsync()
        {
            using (var context = new SupportRequestDbContext())
            {
                return await context.Organization.ToListAsync();
            }
        }
        public async Task<List<ConfigurationItem>> ConfirationItemsAsync()
        {
            using (var context = new SupportRequestDbContext())
            {
                return
                    await context.ConfigurationItem
                        .Include(ci => ci.CIOrganizationIdNavigation)
                        .Include(ci => ci.CiRequestSupportIdNavigation)
                        .ToListAsync();
            }
        }


        public async Task<List<RequestSupport>> RequestSupportsAsync()
        {
            using (var context = new SupportRequestDbContext())
            {
                return
                  await context.RequestSupport
                        .Include(ci => ci.RequestConfigurationItemIdNavigation)
                        .Include(srv => srv.RequestSupportSrvIdNavigation)
                        .ToListAsync();
            }
        }

        public List<RequestSupport> RequestSupports
        {
            get
            {
                using (var context = new SupportRequestDbContext())
                {
                    return
                        context.RequestSupport
                            .Include(req => req.RequestConfigurationItemIdNavigation)
                                .ThenInclude(req => req.CIOrganizationIdNavigation)
                            .Include(req => req.RequestSupportSrvIdNavigation)
                                .ThenInclude(req => req.SRVOrganizationIdNavigation)
                            .ToList();
                }
            }
        }

        public async Task<List<SRV>> SrvsListAsync()
        {
            using (var context = new SupportRequestDbContext())
            {
                return
                    await context.SRV
                        .Include(srv => srv.SRVOrganizationIdNavigation)
                        .Include(srv => srv.SrvRequestSupportNavigation)
                        .ToListAsync();
            }
        }


        public List<SRV> SrvsList()
        {
            return
                context.SRV
                     .Include(srv => srv.SRVOrganizationIdNavigation)
                        .ThenInclude(org => org.OrganizationCiIdNavigation)
                     .Include(srv => srv.SrvRequestSupportNavigation)                     
                     .ToList();
        }

        public async Task AddRecordConfirationItemAsync(ConfigurationItem record)
        {
            using (var context = new SupportRequestDbContext())
            {
                await context.ConfigurationItem.AddAsync(record);
                context.SaveChanges();
            }
        }

        public async Task AddRecordOrganizationAsync(Organization organizatoin)
        {
            using (var context = new SupportRequestDbContext())
            {
                await context.Organization.AddAsync(organizatoin);
                context.SaveChanges();
            }
        }

        public async Task AddRecordRequestSupportAsync(RequestSupport record)
        {
            using (var context = new SupportRequestDbContext())
            {
                await context.RequestSupport.AddAsync(record);
                context.SaveChanges();
            }
        }

        public async Task AddRecordsRequestSupportAsync(List<RequestSupport> records)
        {
            using (var context = new SupportRequestDbContext())
            {
                await context.RequestSupport.AddRangeAsync(records);
                context.SaveChanges();
            }
        }

        public void DeleteRecordsRequestSupport()
        {
            using (var context = new SupportRequestDbContext())
            {
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [RequestSupport]");
            }
        }

        public async Task AddRecordSrvAsync(SRV record)
        {
            using (var context = new SupportRequestDbContext())
            {
                await context.SRV.AddAsync(record);
                context.SaveChanges();
            }
        }

        public async Task AddRecordsOrganizationAsync(List<Organization> organizations)
        {
            using (var context = new SupportRequestDbContext())
            {
                await context.Organization.AddRangeAsync(organizations);
                context.SaveChanges();
            }
        }

        public async Task AddRecordsConfirationItemAsync(List<ConfigurationItem> confirationItems)
        {
            using (var context = new SupportRequestDbContext())
            {
                await context.ConfigurationItem.AddRangeAsync(confirationItems);
                context.SaveChanges();
            }
        }

        public async Task AddRecordsSrvsAsync(List<SRV> srvs)
        {
            using (var context = new SupportRequestDbContext())
            {
                await context.SRV.AddRangeAsync(srvs);
                context.SaveChanges();
            }
        }

        public List<ConfigurationItem> ConfirationItems()
        {
            return
                context.ConfigurationItem
                    .Include(ci => ci.CIOrganizationIdNavigation)
                         .ThenInclude(ci => ci.OrganizationSrvNavigation)
                    .Include(ci => ci.CiRequestSupportIdNavigation)
                    .ToList();
        }
    }
}
