﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EntityAndXML.Model.Interfaces
{
    interface IConfirationItem 
    {
        Task<List<ConfigurationItem>> ConfirationItemsAsync();

        /// <summary>
        /// Добавление новой записи 
        /// </summary>  
        /// <returns></returns>
        Task AddRecordConfirationItemAsync(ConfigurationItem  record);

        Task AddRecordsConfirationItemAsync(List<ConfigurationItem> confirationItems);
    }
}
