﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EntityAndXML.Model.Interfaces
{
    interface IRequestSupport
    {
        List<RequestSupport> RequestSupports { get; }

        Task<List<RequestSupport>> RequestSupportsAsync();

        /// <summary>
        /// Добавление новой записи 
        /// </summary>  
        /// <returns></returns>
        Task AddRecordRequestSupportAsync(RequestSupport record);

        Task AddRecordsRequestSupportAsync(List<RequestSupport> records);
    }
}
