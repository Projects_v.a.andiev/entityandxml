﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityAndXML.Model.Interfaces
{
    public interface IOrganization
    {
        Task<List<Organization>> OrganizationsListAsync();

        /// <summary>
        /// Добавление новой записи 
        /// </summary>  
        /// <returns></returns>
        Task AddRecordOrganizationAsync(Organization record);

        Task AddRecordsOrganizationAsync(List<Organization> records);
    }
}
