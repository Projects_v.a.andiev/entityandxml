﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EntityAndXML.Model.Interfaces
{
    interface ISrv
    {
        Task<List<SRV>> SrvsListAsync();

        /// <summary>
        /// Добавление новой записи 
        /// </summary>  
        /// <returns></returns>
        Task AddRecordSrvAsync(SRV record);

        Task AddRecordsSrvsAsync(List<SRV> srvs);
    }
}
