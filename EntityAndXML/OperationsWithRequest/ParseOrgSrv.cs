﻿using EntityAndXML.FolderFileOperation;
using EntityAndXML.Helper;
using EntityAndXML.Model;
using EntityAndXML.Request;
using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EntityAndXML.OperationsWithRequest
{
    public class ParseOrgSrv
    {
        private RepositoryModel _repositoryModel;
        private ILog _log;
        public ParseOrgSrv(RepositoryModel repositoryModel, ILog log)
        {
            _repositoryModel = repositoryModel;
            _log = log;
        }

        public void ParseOrgSrvObjParallel(string path, bool isRemoveAfterParse)
        {
            var requestORG_SRV = GetRequests(path, isRemoveAfterParse);

            AddOrganizationsAsync(ParseOrganization(requestORG_SRV));
            var indexCi = 0;
            var indexSrv = 0;

            var taskCI = Task.Factory.StartNew(() =>
               AddConfirationItemsAsync(
                   SetOrgIdConfirationItem(ParseConfigurationItem(requestORG_SRV, ref indexSrv).Values.ToList())));

            var taskSRV = Task.Factory.StartNew(() =>
                AddSrvsAsync(
                   SetOrgIdSrv(ParseSRV(requestORG_SRV, ref indexCi).Values.ToList())));

            Task.WaitAll(taskCI, taskSRV);
        }

        public ConcurrentDictionary<int, CIM> GetRequests(string path, bool isRemoveAfterParse)
        {
            try
            {
                var concurrentDictionary = new ConcurrentDictionary<int, CIM>();
                var xmloperation = new XMLOperation();
                var files = Directory.GetFiles(path, "*.xml");

                Parallel.For(0, files.Length,
                index =>
                {
                    concurrentDictionary.TryAdd(index, xmloperation.DeSerialize<CIM>(files[index]));
                    if (isRemoveAfterParse)
                    {
                        DeleteRequestXML(files[index]);
                    }
                });

                return concurrentDictionary;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }


        public void DeleteRequestXML(string pathFile)
        {
            try
            {
                File.Delete(pathFile);
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }


        public ConcurrentDictionary<int, SRV> ParseSRV(ConcurrentDictionary<int, CIM> requests, ref int index)
        {
            try
            {
                var Srvs = new ConcurrentDictionary<int, SRV>();
                foreach (var request in requests)
                {
                    foreach (var group in request.Value.DECLARATION.DECLGROUP)
                    {
                        if (group.INSTANCE.CLASSNAME.Equals("srv", StringComparison.InvariantCultureIgnoreCase))
                        {
                            var i = Interlocked.Increment(ref index);
                            Srvs.TryAdd(i, GetSRV(group.INSTANCE.PROPERTY));

                        }
                    }
                }
                return Srvs;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public async void AddSrvsAsync(List<SRV> srvs)
        {
            try
            {
                await _repositoryModel.AddRecordsSrvsAsync(srvs);
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }



        public ConcurrentDictionary<int, ConfigurationItem> ParseConfigurationItem(ConcurrentDictionary<int, CIM> requests, ref int index)
        {
            try
            {
                var confirationItems = new ConcurrentDictionary<int, ConfigurationItem>();
                foreach (var request in requests)
                {
                    foreach (var group in request.Value.DECLARATION.DECLGROUP)
                    {
                        if (group.INSTANCE.CLASSNAME.Equals("ci", StringComparison.InvariantCultureIgnoreCase))
                        {
                            var i = Interlocked.Increment(ref index);
                            confirationItems.TryAdd(i, GetConfirationItem(group.INSTANCE.PROPERTY));
                        }
                    }
                }
                return confirationItems;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public async void AddConfirationItemsAsync(List<ConfigurationItem> configurationItems)
        {
            try
            {
                await _repositoryModel.AddRecordsConfirationItemAsync(configurationItems);
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }


        public List<Organization> ParseOrganization(ConcurrentDictionary<int, CIM> requests)
        {
            try
            {
                var organizations = new List<Organization>();
                foreach (var request in requests)
                {
                    foreach (var group in request.Value.DECLARATION.DECLGROUP)
                    {
                        if (group.INSTANCE.CLASSNAME.Equals("org", StringComparison.InvariantCultureIgnoreCase))
                        {
                            organizations.Add(GetOrganization(group.INSTANCE.PROPERTY));
                        }
                    }
                }
                return organizations;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }


        public async void AddOrganizationsAsync(List<Organization> organizations)
        {
            try
            {
                await _repositoryModel.AddRecordsOrganizationAsync(organizations);
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public Organization GetOrganization(CIMDECLARATIONVALUEOBJECTINSTANCEPROPERTY[] organization)
        {
            try
            {
                var org = new Organization();
                foreach (var item in organization)
                {
                    switch (item.NAME.ToUpper())
                    {
                        case "ORG_NAME":
                            org.ORG_NAME = item.VALUE;
                            break;
                        case "ORG_SEARCHCODE":
                            org.ORG_SEARCHCODE = item.VALUE;
                            break;
                        case "RCT_NAME":
                            org.RCT_NAME = item.VALUE;
                            break;
                        case "ORG_OID":
                            org.ORG_OID = int.Parse(item.VALUE);
                            break;
                        case "ID_ORG":
                            org.ID_ORG = int.Parse(item.VALUE);
                            break;
                    }
                }
                return org;

            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }


        public ConfigurationItem GetConfirationItem(CIMDECLARATIONVALUEOBJECTINSTANCEPROPERTY[] confirationItems)
        {
            try
            {
                var confirationItem = new ConfigurationItem();
                foreach (var confItem in confirationItems)
                {
                    switch (confItem.NAME.ToUpper())
                    {
                        case "MODEL":
                            confirationItem.MODEL = confItem.VALUE;
                            break;
                        case "NAME":
                            confirationItem.NAME = confItem.VALUE;
                            break;
                        case "ORG_OID":
                            confirationItem.ORG_OID = int.Parse(confItem.VALUE);
                            break;
                        case "SC":
                            confirationItem.SC = confItem.VALUE;
                            break;
                        case "ADR":
                            confirationItem.ADR = confItem.VALUE;
                            break;
                        case "TOWN":
                            confirationItem.TOWN = confItem.VALUE;
                            break;
                        case "TID":
                            confirationItem.TID = confItem.VALUE;
                            break;
                        case "SERNUM":
                            confirationItem.SERNUM = confItem.VALUE;
                            break;
                        case "CAT":
                            confirationItem.CAT = confItem.VALUE;
                            break;
                        case "BLOCKED":
                            confirationItem.BLOCKED = confItem.VALUE;
                            break;
                        case "ID":
                            confirationItem.ID_CI = int.Parse(confItem.VALUE);
                            break;
                    }
                }

                //confirationItem.OrganizationId = GetOrganizationID(confirationItem.ORG_OID);
                return confirationItem;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public List<ConfigurationItem> SetOrgIdConfirationItem(List<ConfigurationItem> configurationItems)
        {
            var confItems = new List<ConfigurationItem>();
            foreach (var items in configurationItems.GroupBy(g => g.ORG_OID))
            {
                foreach (var item in items)
                {
                    item.OrganizationId = GetOrganizationID(item.ORG_OID);
                }
                confItems.AddRange(items);
            }

            return configurationItems;
        }

        public List<SRV> SetOrgIdSrv(List<SRV> sRvs)
        {
            var srvItems = new List<SRV>();
            foreach (var items in sRvs.GroupBy(g => g.ORG_OID))
            {
                foreach (var item in items)
                {
                    item.OrganizationId = GetOrganizationID(item.ORG_OID);
                }
                srvItems.AddRange(items);
            }

            return srvItems;
        }


        public SRV GetSRV(CIMDECLARATIONVALUEOBJECTINSTANCEPROPERTY[] srvs)
        {
            try
            {
                var srv = new SRV();
                foreach (var srvItem in srvs)
                {
                    switch (srvItem.NAME.ToUpper())
                    {
                        case "ID":
                            srv.ID_SRV = int.Parse(srvItem.VALUE);
                            break;
                        case "SRV_NAME":
                            srv.SRV_NAME = srvItem.VALUE;
                            break;
                        case "SRV_ID":
                            srv.SRV_ID = int.Parse(srvItem.VALUE);
                            break;
                        case "PATH":
                            srv.PATH = srvItem.VALUE;
                            break;
                        case "ORG_OID":
                            srv.ORG_OID = int.Parse(srvItem.VALUE);
                            break;
                    }
                }

                //srv.OrganizationId = GetOrganizationID(srv.ORG_OID);

                return srv;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public int? GetOrganizationID(int orgOid)
        {
            try
            {
                var orgId = _repositoryModel.OrganizationsListAsync().GetAwaiter().GetResult().Where(w => w.ORG_OID == orgOid).FirstOrDefault();
                if (orgId is null)
                    return null;

                return orgId.ID;

            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }
    }
}

