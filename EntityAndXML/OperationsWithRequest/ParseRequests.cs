﻿using EntityAndXML.FolderFileOperation;
using EntityAndXML.Model;
using EntityAndXML.Request;
using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntityAndXML.OperationsWithRequest
{
    public class ParseRequests : ParseOrgSrv
    {
        private RepositoryModel _repositoryModel;
        private ILog _log;
        public ParseRequests(RepositoryModel repositoryModel, ILog log) : base(repositoryModel, log)
        {
            _repositoryModel = repositoryModel;
            _log = log;
        }

        public void RunParseRequests(string path, bool isRemoveAfterParse)
        {
            var supportRequests = GetRequests(path, isRemoveAfterParse);

            SaveSupportRequests(FkForRequestSupportSrvIdCiId(ListSupportRequest(supportRequests)));
        }

        public List<RequestSupport> ListSupportRequest(ConcurrentDictionary<int, CIM> supportRequests)
        {
            try
            {
                var requests = new List<RequestSupport>();
                foreach (var item in supportRequests.Values)
                {
                    foreach (var group in item.DECLARATION.DECLGROUP)
                    {
                        if (group.INSTANCE.CLASSNAME.ToLower().Equals("new"))
                        {
                            var requestSupport = RequestSupport(group.INSTANCE.PROPERTY);
                            requests.Add(requestSupport);
                        }
                    }
                }
                return requests;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public async void SaveSupportRequests(List<RequestSupport> requestSupports)
        {
            try
            {
              await  _repositoryModel.AddRecordsRequestSupportAsync(requestSupports);
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }


        private RequestSupport RequestSupport(CIMDECLARATIONVALUEOBJECTINSTANCEPROPERTY[] requests)
        {
            try
            {
                var req = new RequestSupport();
                foreach (var request in requests)
                {
                    switch (request.NAME.ToUpper())
                    {
                        case "ID_HD":
                            req.ID_HD = request.VALUE;
                            break;
                        case "INFO":
                            req.INFO = request.VALUE;
                            break;
                        case "NEW_ROOM":
                            req.NEW_ROOM = request.VALUE;
                            break;
                        case "SC":
                            req.SC = request.VALUE;
                            break;
                        case "SRV_ID":
                            req.SRV_ID = int.Parse(request.VALUE);
                            break;
                        case "DATE_REG":
                            req.DATE_REG = request.VALUE;
                            break;
                        case "DATE_REG_CLIENT":
                            req.DATE_REG_CLIENT = request.VALUE;
                            break;
                        case "DESIRED_TIME":
                            req.DESIRED_TIME = request.VALUE;
                            break;
                        case "EQUIPMENT":
                            req.EQUIPMENT = request.VALUE;
                            break;
                        default:
                            break;
                    }
                }
                //req.SrvId = GetSrvId(req.SRV_ID);
                return req;

            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }

        public List<RequestSupport> FkForRequestSupportSrvIdCiId(List<RequestSupport> requestSupports)
        {
            var req = new List<RequestSupport>();
            foreach (var request in requestSupports)
            {
                request.SrvId = GetSrvId(request.SRV_ID);
                request.ConfigurationItemId = GetConfigurationItemId(request.SC);
                req.Add(request);
            }
            return req;
        }

        public int? GetSrvId(int srvId)
        {
            try
            {
                var org = _repositoryModel.SrvsList().Where(w => w.SRV_ID == srvId).FirstOrDefault();
                if (org is null)
                    return null;

                return org.ID;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }


        public int? GetConfigurationItemId(string searchCode)
        {
            try
            {
                var configurationItem = _repositoryModel.ConfirationItems().Where(w => w.SC == searchCode).FirstOrDefault();
                if (configurationItem is null)
                    return null;

                return configurationItem.ID;
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }
    }
}
