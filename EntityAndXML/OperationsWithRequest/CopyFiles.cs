﻿using EntityAndXML.FolderFileOperation;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EntityAndXML.OperationsWithRequest
{
    public class CopyFiles
    {
        private ILog _log;
        public CopyFiles( ILog log)
        {
            _log = log;
        }

        public void CreateTestFiles()
        {
            try
            {
                var path = PathSettings.PathForFolderRequests();

                Parallel.For(0, 4000,
                 index =>
                 {
                     File.Copy($@"{path}\20191016090152_hd2.xml", $@"{path}\{index}.xml");
                 });
            }
            catch (Exception ex)
            {
                _log.Error(ex.GetBaseException().ToString());
                throw ex;
            }
        }
    }
}
