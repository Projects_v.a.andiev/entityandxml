﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace EntityAndXML.Helper
{
    public class XMLOperation
    {

        public void Serialize<T>(string path, T type)
        {
            var serializer = new XmlSerializer(type.GetType());

            using (var writer = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(writer, type);
            }
        }

        public T DeSerialize<T>(string path) where T : class
        {
            T type;
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = XmlReader.Create(path, new XmlReaderSettings() { DtdProcessing = DtdProcessing.Ignore }))
            {
                reader.MoveToContent();
                type = serializer.Deserialize(reader) as T;
            }
            return type;
        }


    }
}
