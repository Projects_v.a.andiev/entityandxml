﻿using log4net;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace EntityAndXML.Helper
{
    public class Logger
    {
        public ILog InitializeLogger()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            log4net.Config.XmlConfigurator.Configure(logRepository);
            return LogManager.GetLogger(typeof(object));
        }
    }
}