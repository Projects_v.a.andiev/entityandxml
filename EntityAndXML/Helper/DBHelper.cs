﻿
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using EntityAndXML.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityAndXML.Helper
{
   public static class DBHelper
    {
        public static bool CheckDbCOnnection()
        {
            using (var dbContext = new SupportRequestDbContext())
            {
                //Запуск миграций
                dbContext.Database.EnsureCreated();
                //Провверка соединения
                return (dbContext.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists();             
              
            }
        }
    }
}