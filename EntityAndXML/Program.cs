﻿using EntityAndXML.FolderFileOperation;
using EntityAndXML.Helper;
using EntityAndXML.OperationsWithRequest;
using log4net;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EntityAndXML
{
    class Program
    {
        static void Main(string[] args)
        {
            var program = new Program();
            var loggerInstance = new Logger().InitializeLogger();
            //var copyFiles = new CopyFiles();

            //copyFiles.CreateTestFiles();

            if (!DBHelper.CheckDbCOnnection())
            {
                Console.WriteLine("Нет соединения с БД!");
                Console.ReadKey();
                Environment.Exit(0);
            }

            var rep = new Model.RepositoryModel(new Model.SupportRequestDbContext());

            var req = rep.RequestSupports;

            var Cis = rep.ConfirationItems();

            var srvs = rep.SrvsList();

            var parseRequests = new ParseRequests(new Model.RepositoryModel(new Model.SupportRequestDbContext()), loggerInstance);

            parseRequests.ParseOrgSrvObjParallel(PathSettings.PathORG_SRV(),true);

            parseRequests.RunParseRequests(PathSettings.PathForFolderRequests(), true);




            Console.WriteLine("Hello World!");
        }
    }
}
